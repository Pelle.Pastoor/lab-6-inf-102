package INF102.lab6.cheapFlights;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Stack;

import INF102.lab6.graph.DirectedEdge;
import INF102.lab6.graph.WeightedDirectedGraph;

public class CheapestFlight implements ICheapestFlight {

    HashSet<City> visited;
    PriorityQueue<DirectedEdge<City>> queue;
    HashMap<City, Integer> costToCity;
    HashMap<City, Integer> stopsToCity;

    @Override
    public WeightedDirectedGraph<City, Integer> constructGraph(List<Flight> flights) {
        WeightedDirectedGraph<City, Integer> wdg = new WeightedDirectedGraph<>();

        for (Flight flight : flights) {
            wdg.addVertex(flight.start);
            wdg.addVertex(flight.destination);

            wdg.addEdge(flight.start, flight.destination, flight.cost);
        }

        return wdg;
    }

    @Override
    public int findCheapestFlights(List<Flight> flights, City start, City destination, int nMaxStops) {
        WeightedDirectedGraph<City, Integer> graph = constructGraph(flights);

        queue = new PriorityQueue<>(graph);
        visited = new HashSet<>();
        stopsToCity = new HashMap<>();
        costToCity = new HashMap<>();
        
        addCity(graph, start, 0, -1);
        
        while(!queue.isEmpty()){
            DirectedEdge<City> flight = queue.poll();

            if(visited.contains(destination)) break;
            
            int cost = costToCity.get(flight.from) + graph.getWeight(flight);
            int stops = stopsToCity.get(flight.from) + 1;

            if(stops > nMaxStops) continue;

            City nextCity;
            if(visited.contains(flight.from)){
                nextCity = flight.to;
            }else {
                nextCity = flight.from;
            }
            
            addCity(graph, nextCity, cost, stops);
        }

        return costToCity.get(destination);
    }

    private void addCity(WeightedDirectedGraph<City, Integer> graph, City c, int cost, int stops){
        stopsToCity.put(c, stops);
        costToCity.put(c, cost);
        visited.add(c);

        addFlights(graph, c);
    }

    private void addFlights(WeightedDirectedGraph<City, Integer> graph, City c){
        for (DirectedEdge<City> arc : graph.outArcs(c)) {
            queue.add(arc);
        }
    }

}
